package speech.text;

import com.google.common.io.Resources;
import java.io.File;

public class Transcriber {

  public static void main(String[] args) throws Exception {
    String userId = args[0], userToken = args[1];
    DeepgramClient client = new DeepgramClient(userId, userToken);

    System.out.println("Transcribing file...");
    String res2 = client.transcribeSync(new File(Resources.getResource("JonesMedical.mp3").getPath()));

    System.out.println("Transcribing given url...");
    String res1 = client.transcribeSync("http://www.nch.com.au/scribe/practice/JonesMedical.mp3");

    System.out.println("Transcript: \n" + res2);
  }
}
