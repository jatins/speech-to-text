package speech.text.exception;

import org.json.JSONObject;

/**
 * Exception indicating error in transcription
 */
public class TranscriptionException extends RuntimeException {
  private JSONObject httpResponseContent;

  public TranscriptionException(String message) {
    super(message);
  }

  public TranscriptionException(JSONObject httpResponseContent) {
    super(httpResponseContent.toString());
    this.httpResponseContent = httpResponseContent;
  }

  public JSONObject getHttpResponseContent() {
    return httpResponseContent;
  }

  public TranscriptionException(String message, Throwable cause) {
    super(message, cause);
  }
}
