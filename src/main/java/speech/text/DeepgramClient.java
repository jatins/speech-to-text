package speech.text;

import com.google.common.base.Preconditions;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Base64;
import java.util.stream.Collectors;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.json.JSONObject;
import speech.text.exception.TranscriptionException;


public class DeepgramClient {
  private static final String ENDPOINT = "https://brain.deepgram.com/speech:recognize";

  private String base64EncodedCreds;

  public DeepgramClient(String userId, String userToken) {
    String creds = userId + ":" + userToken;
    this.base64EncodedCreds = Base64.getEncoder().encodeToString(creds.getBytes());
  }

  public String transcribeSync(String audioUrl) throws IOException {
    Response response = Request.Post(ENDPOINT)
        .addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION, "Basic " + base64EncodedCreds))
        .addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString()))
        .body(new StringEntity(getBody(audioUrl).toString(), ContentType.APPLICATION_JSON))
        .execute();

    HttpResponse httpResponse = response.returnResponse();
    if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
      throw new TranscriptionException("Non-OK response from DeepGram API. URL: " + audioUrl);
    }

    return handleSuccesfullResponse(httpResponse).getString("transcript");
  }

  public String transcribeSync(File audioFile) throws IOException {
    Preconditions.checkArgument(audioFile.exists(), "Audio file does not exist. Path: " + audioFile.getPath());

    Response response = Request.Post(ENDPOINT)
        .addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION, "Basic " + base64EncodedCreds))
        .addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_OCTET_STREAM.toString()))
        .bodyFile(audioFile, ContentType.APPLICATION_OCTET_STREAM)
        .execute();

    HttpResponse httpResponse = response.returnResponse();
    if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
      throw new TranscriptionException("Non-OK response from DeepGram API. File Path: " + audioFile.getAbsolutePath());
    }

    return handleSuccesfullResponse(httpResponse).getString("transcript");
  }

  // handles 200 OK response
  private static JSONObject handleSuccesfullResponse(HttpResponse httpResponse) throws IOException {
    String content = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()))
        .lines()
        .collect(Collectors.joining("\n"));

    JSONObject responseBody = new JSONObject(content);
    if (responseBody.has("status") &&
        responseBody.getString("status").equals("failed")) {
      throw new TranscriptionException(responseBody);
    }

    return responseBody;
  }

  private static JSONObject getBody(String audioUrl) {
    JSONObject body = new JSONObject()
        .put("config", new JSONObject())
        .put("audio", new JSONObject());

    body.getJSONObject("config").put("async", false);
    body.getJSONObject("audio").put("url", audioUrl);

    return body;
  }
}
